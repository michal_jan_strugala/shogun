package model.board;

public enum ProvinceColor {

	GREEN, RED, BROWN, PURPLE, YELLOW, NONE
}
