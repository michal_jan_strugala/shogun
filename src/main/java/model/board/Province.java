package model.board;

import java.util.EnumMap;

/** Created on: 10-03-2016
 *  @author majk
 * 
 * Class responsible for holding all information about
 * specified province. It contains methods to update
 * information about ongoing changes to the states 
 * of the province. 
 * 
 * @see java.util.EnumMap;
 */

public class Province {
	
	String name;
	ProvinceColor color;
	EnumMap <BuildingName, Boolean> buildings;
	int goldGain;
	int riceGain;
	int maxBuildings;
	int riotIndex;
	int soldiers;
	Player owner;
	
	public Province( String pName, ProvinceColor pColor,
			int gG, int rG, int mB ) {
		
		name = pName;
		color = pColor;
		buildings = new EnumMap<> ( BuildingName.class );
		initializeBuildings();
		goldGain = gG;
		riceGain = rG;
		maxBuildings = mB;
		riotIndex = 0;
		soldiers = 0;
	}
	
	/**
	 * 
	 * @param building type of new building
	 * @return false if building already present
	 */
	public boolean isBuildingPossible( BuildingName building ) {
		if( buildings.get( building ) == true )
			return false;
		if( buildings.size()>= maxBuildings)
			return false;
		return true;
	}
	
	public void updateRiotIndex( int change ) {
		riotIndex += change;
	}
	
	public void updateSoldiers( int change ) {
		soldiers += change;
	}
	
	/**
	 * @param building Which building type should be updated
	 */
	public void updateBuildings( BuildingName building ) {
		buildings.put( building, true );
	}
	
	public void initializeBuildings() {
		buildings.put( BuildingName.THEATER, false );
		buildings.put( BuildingName.TEMPLE, false );
		buildings.put( BuildingName.CASTLE, false );
	}
	
	/**
	 * 
	 * @param nPlayer new owner of the province
	 * @param army nPlayer's soldiers amount
	 */
	public void changeOwner( Player nPlayer, int army ) {
		owner = nPlayer;
		color = nPlayer.color;
		initializeBuildings();
		soldiers = army;
	}

	@Override
	public String toString() {
		return "Province{" +
				"name=" + name +
				", color=" + color +
				", buildings=" + buildings +
				", goldGain=" + goldGain +
				", riceGain=" + riceGain +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Province province = (Province) o;

		if (goldGain != province.goldGain) return false;
		if (riceGain != province.riceGain) return false;
		if (maxBuildings != province.maxBuildings) return false;
		if (name != null ? !name.equals(province.name) : province.name != null) return false;
		return color == province.color;

	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (color != null ? color.hashCode() : 0);
		result = 31 * result + goldGain;
		result = 31 * result + riceGain;
		result = 31 * result + maxBuildings;
		return result;
	}
	public String getName(){
		return name;
	}
}
