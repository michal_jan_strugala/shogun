package model.board.battleTower;

import model.board.PlayerName;

import java.util.Iterator;
import java.util.Map;
import java.util.Random;

/**
 * Class responsible for simulating fights
 * and holding info about troops not visible
 * for players ( peasants ready to join etc. )
 * 
 * @author majk
 *
 */

public class BattleTower {

	Troops troops;
	Troops lastFight;


	public BattleTower( Troops t ) {
		troops = t;
		lastFight = null;
	}

	/**
	 * Iterates over soldiers map and updates entries
	 * if already present.
	 * Careful : Troops will never less than 0.
	 * 
	 * @param t Troops which should be added
	 */


	public void updateTroops( Troops t ) {
		Iterator <? extends Map.Entry<PlayerName, Integer >> it =
				t.getSoldiers().entrySet().iterator();
		
		while( it.hasNext() ) {
			Map.Entry< PlayerName, Integer > x = it.next();
			PlayerName name = x.getKey();
			if( troops.soldiers.containsKey( name ) ) {
				int cardinality = x.getValue();
				troops.updateSoldiers( name, cardinality );
			}
		}
	}
	
	public void updateTroops( PlayerName name, int change ) {
		if( troops.soldiers.containsKey( name ) ) {
			troops.updateSoldiers( name, change );
		}
	}
	
	/**
	 * Simulates fight for neutral province, peasants'
	 * army is increased by one.
	 * @param attacker Player attacking province
	 * @param attArmy Player's soldiers cardinality
	 * @return survivor soldiers;
	 */
	
	public Troops neutralProvinceAttack( PlayerName attacker, int attArmy ) {
		Troops result =
				simulateFight( attacker, attArmy, PlayerName.PEASANT, 1, false );
		
		return result;
	}
	
	/**
	 * Method updates current troops cardinality
	 * inside tower and returns result of the fight 
	 * between two players. If riot index is neutral
	 * peasants join defender's side.
	 * 
	 * @param attacker attacking Player's name
	 * @param attArmy cardinality of attacking army
	 * @param defender defending Player's name
	 * @param defArmy cardinality of defending army
	 * @param riot riot index negative - peasants join fight
	 * @return Troops of survivors
	 */

	public Troops simulateFight( PlayerName attacker, int attArmy,
			PlayerName defender, int defArmy, boolean riot ) {
		
		Random generator = new Random();
		int peasants, attackers, defenders;
		
		// add armies to the BT
		updateTroops( attacker, attArmy );
		updateTroops( defender, defArmy );
		attackers = troops.soldiers.get( attacker );
		defenders = troops.soldiers.get( defender );
		
		attackers =  generator.nextInt( attackers + 1); 
		defenders =  generator.nextInt( defenders + 1);

		lastFight = new Troops( attacker, defender, PlayerName.PEASANT );
		lastFight.updateSoldiers( attacker, attackers );
		lastFight.updateSoldiers( defender, defenders );
		
		// after fight, cardinality of soldiers inside the BT changes
		updateTroops( attacker, -attackers );
		updateTroops( defender, -defenders );
		
		if( !riot ) {
			peasants = troops.soldiers.get( PlayerName.PEASANT );
			peasants = generator.nextInt( peasants + 1);

			lastFight.updateSoldiers(PlayerName.PEASANT,peasants);

			updateTroops( PlayerName.PEASANT, -peasants );
			int tmp = attackers;
			attackers -= peasants;
			peasants -= tmp;
			
			if( peasants > 0 ){ 
				updateTroops( PlayerName.PEASANT, peasants ); //peasants who survive return to tower
				attackers = 0;
			}
		} 
		
		int tmp = attackers;
		attackers -= defenders;
		defenders -= tmp;
		
		if( attackers < 0 ) attackers = 0;
		if( defenders < 0 ) defenders = 0;
		
		Troops result = new Troops( attacker, defender );
		result.updateSoldiers( attacker, attackers );
		result.updateSoldiers( defender, defenders );
		
		return result;
	}

	public Troops LastFighting(){
		return lastFight;
	}
}
