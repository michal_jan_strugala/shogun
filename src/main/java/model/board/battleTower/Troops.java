package model.board.battleTower;

import model.board.PlayerName;

import java.util.EnumMap;

/**
 * Class used mainly for battle tower.
 * Holds information about troops of all
 * users.
 * 
 * @author majk
 *
 */

public class Troops {

	EnumMap <PlayerName, Integer > soldiers;
	
	public Troops( PlayerName... names ) {
		soldiers = new EnumMap < PlayerName, Integer > ( PlayerName.class );
		for( PlayerName name : names ) {
			soldiers.put( name, 0 );
		}
	}
	
	public Troops( EnumMap < PlayerName, Integer > s ) {
		soldiers = s;
	}
	
	/** Changes current cardinality of soldiers
	 * 
	 * @param name Owner of these soldiers
	 * @param change +/- value
	 */
	
	void updateSoldiers( PlayerName name, int change ) {
		if ( !soldiers.containsKey( name ) )
			return;
		if ( soldiers.get( name ) + change <= 0 )
			soldiers.put( name, 0 );
		else
			soldiers.put( name, soldiers.get( name ) + change );
	}
	
	void updateSoldiers( EnumMap < PlayerName, Integer > s ) {
		soldiers = s;
	}
	
	public EnumMap < PlayerName, Integer > getSoldiers() { return soldiers;	}
}
