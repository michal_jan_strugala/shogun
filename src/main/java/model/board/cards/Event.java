package model.board.cards;

/**
 * Grouping enums in order to organize.
 * These enums describe what EventCard can.
 * @author dominik.
 */
public interface Event {
    enum Limit implements Event{
        RICE_MIN, RICE_MAX, GOLD_MIN, GOLD_MAX
    }
    enum ActionModyfication implements Event{
        CASTLE_DEFENCE,TEMPLE_UNATTACKABLE, NEUTRAL_PROVINCES_DEFENCE, REDUCING_RECRUITMENT
    }
}
