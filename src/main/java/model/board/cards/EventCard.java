package model.board.cards;

/**
 * EventCard hold information about
 * event of season and
 * winter consumption of rice.
 * @author dominik.
 */
public class EventCard implements Card{
    private final int winterConsumption;
    private final Event event;

    public EventCard(int winterConsumption, Event event) {
        this.winterConsumption = winterConsumption;
        this.event = event;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventCard eventCard = (EventCard) o;

        if (winterConsumption != eventCard.winterConsumption) return false;
        return event != null ? event.equals(eventCard.event) : eventCard.event == null;

    }

    @Override
    public int hashCode() {
        int result = winterConsumption;
        result = 31 * result + (event != null ? event.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EventCard{" +
                "winterConsumption=" + winterConsumption +
                ", event=" + event +
                '}';
    }
}
