package model.board.cards;

/**
 * Grouping enums in order to organize.
 * These enums describe what ActionCard can.
 * @author dominik.
 */
public interface Action {
    enum Recruit implements Action{
        FIVE,THREE,ONE
    }
    enum Build implements Action{
        CASTLE, TEMPLE, THEATRE
    }
    enum Move implements Action{
        A,B
    }
    enum Collect implements Action{
        RICE, GOLD
    }
}
