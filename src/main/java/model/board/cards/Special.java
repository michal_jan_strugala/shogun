package model.board.cards;

/**
 * Enum on bonuses.
 * @author dominik.
 */
public enum Special {
    ATTACK, DEFENSE, GOLD, RICE, RECRUIT
}

