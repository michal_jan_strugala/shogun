package model.board.cards;

/**
 * SpecialCard hold information only about
 * bonus.
 * @author dominik.
 */
public class SpecialCard implements Card{
    private final Special bonus;

    public SpecialCard(Special bonus) {
        this.bonus = bonus;
    }

    public Special getBonus() {
        return bonus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpecialCard that = (SpecialCard) o;

        return bonus == that.bonus;

    }

    @Override
    public int hashCode() {
        return bonus != null ? bonus.hashCode() : 0;
    }
}
