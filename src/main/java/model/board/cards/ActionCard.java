package model.board.cards;

/**
 * ActionCard hold information about
 * action and its cost.
 * @author dominik.
 */
public class ActionCard implements Card{
    private final int cost;
    private final Action action;

    public ActionCard(Action action, int cost) {
        this.action = action;
        this.cost =cost;
    }

    public Action getAction() {
        return action;
    }

    public int getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return "ActionCard{" +
                "cost=" + cost +
                ", action=" + action +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActionCard that = (ActionCard) o;

        if (cost != that.cost) return false;
        return action != null ? action.equals(that.action) : that.action == null;

    }

    @Override
    public int hashCode() {
        int result = cost;
        result = 31 * result + (action != null ? action.hashCode() : 0);
        return result;
    }
}
