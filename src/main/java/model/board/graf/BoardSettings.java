package model.board.graf;

public interface BoardSettings {
    enum Size implements  BoardSettings {
        SMALLER, BIGGER
    }
    enum DayTime implements  BoardSettings {
        DAY, NIGHT
    }
}
