package model.board.graf;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import model.board.Province;

/** Created on: 26-04-2016
 *  @author dominik
 *
 * This is a single node of Graf.
 * GrafNode contain the province
 * and neighbors (connections to other provinces)
 * of this province.
 *
 * @see Graf
 */
class GrafNode {
    private HashSet<GrafNode> neighbors = new HashSet<GrafNode>();
    private final Province province;

    /** Constructor.
     *
     * @param province The main element of graph node, Province.
     */
    public GrafNode(Province province) {
        this.province = province;
    }

    /**
     *
     * @return Neighbors of this node(province)
     */
    public List<Province> getNeighbors() {
        List<Province> list = new ArrayList<Province>();
        for(Object node : this.neighbors.toArray()){
            list.add(((GrafNode)node).getProvince());
        }
        return list;
    }

    public Province getProvince() {
        return province;
    }

    /**Check if another graph node has connection to this node.
     *
     * @param province another graph node.
     * @return true if this province and the another one has connection
     */
    public boolean isNeighbor(GrafNode province){
        for(GrafNode node : neighbors){
            if(node.equals(province))
                return true;
        }
        return false;
    }

    /** Check if provinces are next to  each other
     *
     * @param province another province
     * @return true if this province and the another one has connection
     */
    public boolean isNeighbor(Province province){
        for(GrafNode node : neighbors){
            if(node.getProvince().equals(province))
                return true;
        }
        return false;
    }
    /** Check if provinces representing by name are next to  each other
     *
     * @param province another province
     * @return true if this province and the another one has connection
     */
    public boolean isNeighbor(String province){
        for(GrafNode node : neighbors){
            if(node.getProvince().getName().equals(province))
                return true;
        }
        return false;
    }

    /** Adding neighbor, province which is adjacent to this one.
     *
     * @param grafNode graph node which has connection to this node.
     */
    public void AddNeighbor(GrafNode grafNode){
        neighbors.add(grafNode);
    }

    /** Remove one of neighbors from the list.
     *
     * @param grafNode The graph node which we want to remove from neighbors list.
     * @return true if everything is successful
     */
    public boolean DelNeighbor(GrafNode grafNode){
        return neighbors.remove(grafNode);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GrafNode grafNode = (GrafNode) o;

        return province != null ? province.equals(grafNode.province) : grafNode.province == null;

    }

    @Override
    public int hashCode() {
        return province != null ? province.hashCode() : 0;
    }


}
