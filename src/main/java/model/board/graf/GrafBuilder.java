package model.board.graf;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import model.board.Province;
import model.board.ProvinceColor;

/** Created on: 26-04-2016
 *  @author dominik
 *
 *  It is the graph builder. It was created to  separate
 *  functionality of the graf from the constructing object.
 *
 *  @see Graf
 */
public class GrafBuilder {

    private boolean isCommentOrBlank(String line){return line.length()==0 || line.trim().charAt(0)== '#'; }

    private boolean isProvinceBeginning(String line) {
        return line.contains(":");
    }

    private boolean isColorDefinition(String line){
        return line.contains(">");
    }

    private BoardSettings string2settings (String str){
        List<String> sizeEnums = new ArrayList<>();
        List<String> dayTimeEnums = new ArrayList<>();

        for(BoardSettings.Size s : BoardSettings.Size.values()){
            sizeEnums.add(s.name());
        }
        for(BoardSettings.DayTime d : BoardSettings.DayTime.values()){
            dayTimeEnums.add(d.name());
        }

        if(sizeEnums.contains(str)){
            return BoardSettings.Size.valueOf(str);
        }
        else if (dayTimeEnums.contains(str)){
            return BoardSettings.DayTime.valueOf(str);
        }
        else
            return null;
    }

    private List<BoardSettings> getProvinceSettings(String settings){
        List<BoardSettings> listOfSettings = new ArrayList<BoardSettings>();
        if(settings.length()!=0 && settings!=null) {
            for (String s : settings.split(",")) {
                listOfSettings.add(string2settings(s));
            }
        }
        return listOfSettings;
    }

    private boolean checkSettings(String boardSettings, BoardSettings.DayTime dayTime, BoardSettings.Size boardSize) {
        List<BoardSettings> provinceSettings = getProvinceSettings(boardSettings);

        BoardSettings.DayTime provinceDayTime = null;
        BoardSettings.Size provinceSize = null;

        for(BoardSettings bS : provinceSettings){
            if(bS instanceof BoardSettings.DayTime)
                provinceDayTime = (BoardSettings.DayTime) bS;
            else if(bS instanceof  BoardSettings.Size)
                provinceSize = (BoardSettings.Size) bS;
        }

        if(provinceDayTime != null && !provinceDayTime.equals(dayTime))
            return false;
        if(provinceSize != null && !provinceSize.equals(boardSize))
            return false;
        return true;
    }

    // kolejnosc gold, rice , buildings jest istotna
    private List<GrafNode> getGrafNodes(File ProvinceSettings, BoardSettings.DayTime dayTime , BoardSettings.Size boardSize ) throws IOException {
        List<GrafNode> nodesSet = new ArrayList<GrafNode>();
        String line;
        BufferedReader in = new BufferedReader(new FileReader(ProvinceSettings));
        ProvinceColor provinceColor = null;

        while((line = in.readLine())!=null){
            line = line.trim();
            if(isCommentOrBlank(line))
                continue;
            else if(isColorDefinition(line))
                provinceColor = ProvinceColor.valueOf(line.substring(1,line.length())); //line is trimmed
            else if(isProvinceBeginning(line) && checkSettings(line.substring(line.indexOf(":")+1,line.length()).trim() , dayTime , boardSize)){
                String name = line.substring(0,line.indexOf(":")).trim();
                int gold = Integer.parseInt(in.readLine().split("=")[1].trim());
                int rice = Integer.parseInt(in.readLine().split("=")[1].trim());
                int buildings = Integer.parseInt(in.readLine().split("=")[1].trim());

                nodesSet.add(new GrafNode(new Province(name,provinceColor,gold,rice,buildings)));
            }
        }
        in.close();
        return nodesSet;
    }

    private void initConnections(Graf graf , File grafSettings) throws IOException {

        String line;
        BufferedReader in = new BufferedReader(new FileReader(grafSettings));
        while((line = in.readLine())!=null){
            line=line.trim();
            if(isCommentOrBlank(line))
                continue;
            String[] neighbors = line.split("->")[1].split(",");
            String provinceName = line.split("->")[0].trim();

            GrafNode editingNode = graf.getNode(provinceName);

            for(String neighbor : neighbors){
                GrafNode ng = graf.getNode(neighbor.trim());
                editingNode.AddNeighbor(ng);
            }
        }
        in.close();
    }

    /** Create complete graph with all nodes and connections between them.
     *
     * @param grafSettings file where construction of graph is stored
     * @param ProvinceSettings file where all prvinces with details are stored
     * @param dayTime setting about board on which player will play
     * @param boardSize setting about size of board. It representing how big board should be.
     * @return Graf if no exceptions are thrown, which means that everything is ok. In other case it returns null.
     */
    public Graf createGraf(File grafSettings , File ProvinceSettings, BoardSettings.DayTime dayTime , BoardSettings.Size boardSize ){
        Graf graf = new Graf();
        try {
            graf.nodesSet = getGrafNodes(ProvinceSettings,dayTime,boardSize);
            initConnections(graf,grafSettings);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return graf;
    }

    public static void main(String[] args) {

        GrafBuilder gB = new GrafBuilder();

        String grafRscr = "src/main/resources/graphSettings/";
        File grafSettings = new File(grafRscr + "DaySmallerBoard.txt");
        File provincesSettings = new File(grafRscr + "ProvincesSettings.txt");

        Graf  graf = gB.createGraf(grafSettings, provincesSettings  , BoardSettings.DayTime.DAY , BoardSettings.Size.SMALLER);

        if(graf==null)
            System.err.println("graf = null");
        else System.out.println(graf);
    }

}
