package model.board.graf;

import java.util.List;

import model.board.Province;

/** Created on: 26-04-2016
 *  @author dominik
 *
 * This class is a represantation of
 * connections between provinces. By Graf
 * you are able to check connections and to
 * get node.
 *
 * @see GrafNode
 *
 */

public class Graf {

    List<GrafNode> nodesSet ;

    /** This method is responsible for obtaining graph node
     *  from name of province
     *
     * @param provinceName the name of the province
     * @return GrafNode if everything is successful otherwise null
     */
    public GrafNode getNode(String provinceName){
        for(GrafNode n:nodesSet){
            if(n.getProvince().getName().equals(provinceName))
                return n;
        }
        return null;
    }

    /** This method is responsible for obtaining graph node
     *  from province
     *
     * @param province the province representing part of board-map
     * @return GrafNode if everything is successful otherwise null
     */
    public GrafNode getNode(Province province){
        for(GrafNode n:nodesSet){
            if(n.getProvince().equals(province))
                return n;
        }
        return null;
    }

    /** By using this method you are able to check if
     *  two different graph nodes have connection.
     *
     * @param province1 the graph node representing the first province
     * @param province2 the graph node representing the second province
     * @return true if graph nodes have connection otherwise false
     */
    public boolean nextTo(GrafNode province1, GrafNode province2){
        for (GrafNode n : nodesSet){
            if(n.equals(province1)){
                return n.isNeighbor(province2);
            }
        }
        return false;
    }
    /** By using this method you are able to check if provinces
     *  adjacent to each other
     *
     * @param province1 the first province
     * @param province2 the second province
     * @return true if provinces are next to each other otherwise false
     */
    public boolean nextTo(Province province1, Province province2){
        for (GrafNode n : nodesSet){
            if(n.getProvince().equals(province1)){
                return n.isNeighbor(province2);
            }
        }
        return false;
    }
    /** By using this method you are able to check if provinces
     *  represented by name adjacent to each other
     *
     * @param province1 the graph node representing the first province
     * @param province2 the graph node representing the second province
     * @return true if provinces are next to each other otherwise false
     */
    public boolean nextTo(String province1, String province2){
        for (GrafNode n : nodesSet){
            if(n.getProvince().getName().equals(province1)){
                return n.isNeighbor(province2);
            }
        }
        return false;
    }

    @Override
    public String toString() {
        String str = "";
        for(GrafNode node : nodesSet){
            str = str + node.getProvince().getName() + " -> " ;
            for(Province p : node.getNeighbors()){
                str = str + p.getName() + ", ";
            }
            str = str + "\n";
        }
        return str;
    }
}
