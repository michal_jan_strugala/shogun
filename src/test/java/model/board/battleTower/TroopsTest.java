package model.board.battleTower;

import model.board.PlayerName;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TroopsTest {

    @Test
    public void testInitTroops(){
        PlayerName p = PlayerName.DATE_MASAMUMUNE;
        Troops t = new Troops(p);

        int count = t.getSoldiers().get(p);
        assertEquals("Initial value should be 0" , 0 , count);
    }

    @Test
    public void testUpdateThereIsNoPlayerName(){
        PlayerName p = PlayerName.DATE_MASAMUMUNE;
        Troops t = new Troops(p);

        t.updateSoldiers(PlayerName.ODA_NOBUNAGA,2);

        int count = t.getSoldiers().get(p);
        assertEquals("When we add soldiers to unexisting player, number of existing player shouldn't change ", 0, count);
    }

    @Test
    public void testAddTroops1Player(){
        PlayerName p = PlayerName.DATE_MASAMUMUNE;
        Troops t = new Troops(p);

        t.updateSoldiers(p,2);
        int count = t.getSoldiers().get(p);
        assertEquals("0 + 2 must be 2" , 2 , count);
    }

    @Test
    public void testRemoveTroops1Player(){
        PlayerName p = PlayerName.DATE_MASAMUMUNE;
        Troops t = new Troops(p);

        t.updateSoldiers(p,2);
        t.updateSoldiers(p,-1);

        int count = t.getSoldiers().get(p);
        assertEquals(" 2 - 1 should be 1" , 1 , count );
    }
    @Test
    public void testRemoveTroopsMoreThenExist1Player(){
        PlayerName p = PlayerName.DATE_MASAMUMUNE;
        Troops t = new Troops(p);

        t.updateSoldiers(p,2);
        t.updateSoldiers(p,-5);

        int count = t.getSoldiers().get(p);
        assertEquals("Number of soldiers shouldn't be less than 0" , 0 , count );
    }
    @Test
    public void testAddSoldiers3Players(){
        PlayerName q = PlayerName.DATE_MASAMUMUNE, w = PlayerName.ODA_NOBUNAGA , e = PlayerName.TAKEDA_SHINGEN;
        Troops t =new Troops(q,w,e);

        t.updateSoldiers(q,2);
        t.updateSoldiers(w,3);
        t.updateSoldiers(e,4);

        int count_q = t.getSoldiers().get(q);
        int count_w = t.getSoldiers().get(w);
        int count_e = t.getSoldiers().get(e);

        assertEquals("Number of soldiers should be 2" , 2 , count_q);
        assertEquals("Number of soldiers should be 3" , 3 , count_w);
        assertEquals("Number of soldiers should be 4" , 4 , count_e);
    }

    @Test
    public void testRemoveSoldiers3Players(){
        PlayerName q = PlayerName.DATE_MASAMUMUNE, w = PlayerName.ODA_NOBUNAGA , e = PlayerName.TAKEDA_SHINGEN;
        Troops t =new Troops(q,w,e);

        t.updateSoldiers(q,2);
        t.updateSoldiers(w,3);
        t.updateSoldiers(e,4);

        t.updateSoldiers(q,-1);
        t.updateSoldiers(w,-5);
        t.updateSoldiers(e,-4);

        int count_q = t.getSoldiers().get(q);
        int count_w = t.getSoldiers().get(w);
        int count_e = t.getSoldiers().get(e);

        assertEquals("2 - 1 is 1" , 1 , count_q);
        assertEquals("3 - 5 should be 0 " , 0 , count_w);
        assertEquals("4 - 4 is 0" , 0 , count_e);
    }
}