package model.board.battleTower;

import model.board.Player;
import model.board.PlayerName;
import org.junit.Before;
import org.junit.Test;

import java.util.EnumMap;

import static org.junit.Assert.*;

public class BattleTowerTest {

    BattleTower bt;
    @Before
    public void init(){
        bt = new BattleTower(new Troops(PlayerName.DATE_MASAMUMUNE,PlayerName.ODA_NOBUNAGA,PlayerName.PEASANT));
    }
    @Test
    public void initTest(){
        if(bt.troops.getSoldiers().get(PlayerName.DATE_MASAMUMUNE)!=0)
            fail("troops init is wrong");
        if(bt.troops.getSoldiers().get(PlayerName.ODA_NOBUNAGA) != 0)
            fail();
        if(bt.troops.getSoldiers().get(PlayerName.PEASANT) != 0)
            fail();
    }
    @Test
    public void updateTroops1PlayerByPlayerNameTest(){
        bt.updateTroops(PlayerName.DATE_MASAMUMUNE,2);
        int count = bt.troops.getSoldiers().get(PlayerName.DATE_MASAMUMUNE);
        assertEquals("After adding 2 soldiers, in battleTower these 2 soldiers should be there" , 2, count);

        bt.updateTroops(PlayerName.DATE_MASAMUMUNE,-1);
        count =bt.troops.getSoldiers().get(PlayerName.DATE_MASAMUMUNE);
        assertEquals("2 - 1 should be 1" , 1, count);

        bt.updateTroops(PlayerName.DATE_MASAMUMUNE,-2);
        count =bt.troops.getSoldiers().get(PlayerName.DATE_MASAMUMUNE);
        assertEquals("1 - 2 should be 0, numebr of soldiers can't be less than 0" , 0, count);
    }

    @Test
    public void updateTroops2PlayerByTroopsTest(){
        //number of troops can't be less than 0, so by this update we are able only to add

        Troops t = new Troops(PlayerName.DATE_MASAMUMUNE,PlayerName.ODA_NOBUNAGA);
        t.updateSoldiers(PlayerName.DATE_MASAMUMUNE,12);
        t.updateSoldiers(PlayerName.ODA_NOBUNAGA,1);
        bt.updateTroops(t);

        int count1 = bt.troops.getSoldiers().get(PlayerName.DATE_MASAMUMUNE);
        int count2 = bt.troops.getSoldiers().get(PlayerName.ODA_NOBUNAGA);

        assertEquals("After adding 12 soldiers, in battleTower these 12 soldiers should be there" , 12, count1);
        assertEquals("After adding 1 soldiers, in battleTower these 1 soldiers should be there" , 1, count2);

        bt.updateTroops(t);

        count1 = bt.troops.getSoldiers().get(PlayerName.DATE_MASAMUMUNE);
        count2 = bt.troops.getSoldiers().get(PlayerName.ODA_NOBUNAGA);

        assertEquals("12 + 12 should be 24" , 24, count1);
        assertEquals("1 + 1 should be 2" , 2, count2);
    }

    @Test
    public void fightNoPeasantHelpsTest(){
        /*TODO
            very symilar to todos in fight with peasents.

         */
        //initial values
        bt.updateTroops(PlayerName.DATE_MASAMUMUNE,1);
        bt.updateTroops(PlayerName.ODA_NOBUNAGA,2);
        bt.updateTroops(PlayerName.PEASANT,4);

        Troops returned = bt.simulateFight(PlayerName.ODA_NOBUNAGA,6,PlayerName.DATE_MASAMUMUNE,4,true);

        EnumMap<PlayerName,Integer> towerSurvivors = bt.troops.getSoldiers();

        int attackerRand = bt.lastFight.getSoldiers().get(PlayerName.ODA_NOBUNAGA);
        int defenderRand = bt.lastFight.getSoldiers().get(PlayerName.DATE_MASAMUMUNE);

        assertEquals("At the end the number of soldiers inside + outside(rand, fighting) of tower should be equal to former quantity of soldiers inside + new army which we put into tower at the beginning of fight",8,towerSurvivors.get(PlayerName.ODA_NOBUNAGA)+ attackerRand);
        assertEquals("At the end the number of soldiers inside + outside(rand, fighting) of tower should be equal to former quantity of soldiers inside + new army which we put into tower at the beginning of fight",5,towerSurvivors.get(PlayerName.DATE_MASAMUMUNE)+ defenderRand);

        if(defenderRand<attackerRand){
            //If the number of fighting defenders is smaller than invaders ...
            assertEquals(" All defenders should die",0,(int)returned.soldiers.get(PlayerName.DATE_MASAMUMUNE));
            assertEquals(" Number of invaders which are alive should be = attackers - defenders",attackerRand - defenderRand,(int)returned.soldiers.get(PlayerName.ODA_NOBUNAGA));
        }
        else if(defenderRand>attackerRand){
            //If the number of fighting defenders is greater than invaders ...
            assertEquals(" All invaders should die",0,(int)returned.soldiers.get(PlayerName.ODA_NOBUNAGA));
            assertEquals(" Number of defenders which are alive should be = defenders - attackers",defenderRand - attackerRand,(int)returned.soldiers.get(PlayerName.DATE_MASAMUMUNE));
        }
        else if(defenderRand == attackerRand){
            //If the number of fighting defenders is equal to invaders ...
            assertEquals(" All invaders should die",0,(int)returned.soldiers.get(PlayerName.ODA_NOBUNAGA));
            assertEquals(" All defenders should die",0,(int)returned.soldiers.get(PlayerName.DATE_MASAMUMUNE));
        }

    }
    private void fightPeasantHelpAssertations(BattleTower bt, Troops returned, PlayerName attacker, PlayerName defender, PlayerName peasant, int initPeasants) {
        EnumMap<PlayerName, Integer> troopsInTower = bt.troops.getSoldiers();

        int attackerRand = bt.lastFight.getSoldiers().get(attacker);
        int defenderRand = bt.lastFight.getSoldiers().get(defender);
        int peasantRand = bt.lastFight.getSoldiers().get(peasant);


        if (defenderRand + peasantRand < attackerRand) {
            //If the number of fighting defenders + defending peasant is smaller than invaders ...
            assertEquals(" All defenders should die", 0, (int) returned.soldiers.get(defender));
            assertEquals(" Number of invaders should be = attackers - defending peasants - defenders", attackerRand - peasantRand - defenderRand, (int) returned.soldiers.get(attacker));
            assertEquals("All defending peasants should be dead. In the tower should be initial - fighting", initPeasants - peasantRand, (int) troopsInTower.get(peasant));
        } else if (peasantRand == attackerRand) {
            assertEquals("All defenders should alive ", defenderRand, (int) returned.soldiers.get(defender));
            assertEquals("None of peasants should alive. In the should be initial - fighting ", initPeasants - peasantRand, (int) troopsInTower.get(peasant));
            assertEquals("None of attackers should alive", 0, (int) returned.soldiers.get(attacker));
        } else if (peasantRand > attackerRand) {
            assertEquals("All defenders should alive ", defenderRand, (int) returned.soldiers.get(defender));
            assertEquals("Some of peasants should alive. In the tower should be initial - fighting + alive = initial - attacker", initPeasants - attackerRand, (int) troopsInTower.get(peasant));
            assertEquals("All attackers should die", 0, (int) returned.soldiers.get(attacker));
        } else if (peasantRand < attackerRand) {
            assertEquals("Some defenders should be dead", defenderRand - (attackerRand - peasantRand), (int) returned.soldiers.get(defender));
            assertEquals("None of peasants should alive. In the tower should be initial - fighting ", initPeasants - peasantRand, (int) troopsInTower.get(peasant));
            assertEquals("All attackers should be dead", 0, (int) returned.soldiers.get(attacker));
        }
    }
    @Test
    public void fightPeasantHelpsAttackersMoreTest(){
        //initial values
        PlayerName attacker = PlayerName.ODA_NOBUNAGA;
        PlayerName defender = PlayerName.DATE_MASAMUMUNE;
        PlayerName peasant = PlayerName.PEASANT;
        int initAttacker = 2;
        int initDefender = 1;
        int initPeasants = 3;

        bt.updateTroops(defender,initDefender);
        bt.updateTroops(attacker,initAttacker);
        bt.updateTroops(peasant,initPeasants);


        Troops returned = bt.simulateFight(attacker,6,defender,4,false);

        assertEquals("At the end the number of soldiers inside + outside(rand, fighting) of tower should be equal to former quantity of soldiers inside + new army which we put into tower at the beginning of fight", 8, bt.troops.getSoldiers().get(attacker) + bt.lastFight.getSoldiers().get(attacker));
        assertEquals("At the end the number of soldiers inside + outside(rand, fighting) of tower should be equal to former quantity of soldiers inside + new army which we put into tower at the beginning of fight", 5, bt.troops.getSoldiers().get(defender) +  bt.lastFight.getSoldiers().get(defender));

        fightPeasantHelpAssertations(bt,returned,attacker,defender,peasant,initPeasants);
    }
    @Test
    public void fightPeasantHelpsAttackersEqualsToDefenderTest(){
        //initial values
        PlayerName attacker = PlayerName.ODA_NOBUNAGA;
        PlayerName defender = PlayerName.DATE_MASAMUMUNE;
        PlayerName peasant = PlayerName.PEASANT;
        int initAttacker = 2;
        int initDefender = 1;
        int initPeasants = 3;

        bt.updateTroops(defender,initDefender);
        bt.updateTroops(attacker,initAttacker);
        bt.updateTroops(peasant,initPeasants);


        Troops returned = bt.simulateFight(attacker,6,defender,7,false);

        assertEquals("At the end the number of soldiers inside + outside(rand, fighting) of tower should be equal to former quantity of soldiers inside + new army which we put into tower at the beginning of fight", 8, bt.troops.getSoldiers().get(attacker) + bt.lastFight.getSoldiers().get(attacker));
        assertEquals("At the end the number of soldiers inside + outside(rand, fighting) of tower should be equal to former quantity of soldiers inside + new army which we put into tower at the beginning of fight", 8, bt.troops.getSoldiers().get(defender) +  bt.lastFight.getSoldiers().get(defender));

        fightPeasantHelpAssertations(bt,returned,attacker,defender,peasant,initPeasants);
    }
    @Test
    public void fightPeasantHelpsAttackersLessTest(){
        //initial values
        PlayerName attacker = PlayerName.ODA_NOBUNAGA;
        PlayerName defender = PlayerName.DATE_MASAMUMUNE;
        PlayerName peasant = PlayerName.PEASANT;
        int initAttacker = 2;
        int initDefender = 1;
        int initPeasants = 3;

        bt.updateTroops(defender,initDefender);
        bt.updateTroops(attacker,initAttacker);
        bt.updateTroops(peasant,initPeasants);


        Troops returned = bt.simulateFight(attacker,6,defender,12,false);

        assertEquals("At the end the number of soldiers inside + outside(rand, fighting) of tower should be equal to former quantity of soldiers inside + new army which we put into tower at the beginning of fight", 8, bt.troops.getSoldiers().get(attacker) + bt.lastFight.getSoldiers().get(attacker));
        assertEquals("At the end the number of soldiers inside + outside(rand, fighting) of tower should be equal to former quantity of soldiers inside + new army which we put into tower at the beginning of fight", 13, bt.troops.getSoldiers().get(defender) +  bt.lastFight.getSoldiers().get(defender));

        fightPeasantHelpAssertations(bt,returned,attacker,defender,peasant,initPeasants);
    }

    @Test
    public void fightNeutralProvince(){

    }
}