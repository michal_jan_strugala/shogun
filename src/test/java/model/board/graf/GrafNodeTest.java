package model.board.graf;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import model.board.Province;
import model.board.ProvinceColor;

public class GrafNodeTest {

	@Test
	public void testNeighborsList() {
		GrafNode gn = new GrafNode( new Province( "Bingo", ProvinceColor.GREEN, 0, 0, 0 ) );
		GrafNode n1 = new GrafNode( new Province( "Iyo", ProvinceColor.GREEN, 0, 0, 0 ) );
		GrafNode n2 = new GrafNode( new Province( "Hehe", ProvinceColor.NONE, 0, 0, 0 ) );
		
		gn.AddNeighbor( n1 );
		gn.AddNeighbor( n2 );
		
		List <Province> result = new ArrayList<Province>();
		result.add( n1.getProvince() );
		result.add( n2.getProvince() );
		
		Assert.assertEquals( result, gn.getNeighbors() );
	}
	
	@Test
	public void testIfNeighborCorrect() {
		GrafNode gn = new GrafNode( new Province( "Bingo", ProvinceColor.GREEN, 0, 0, 0 ) );
		GrafNode n1 = new GrafNode( new Province( "Iyo", ProvinceColor.GREEN, 0, 0, 0 ) );
		
		gn.AddNeighbor( n1 );
		
		Assert.assertTrue( gn.isNeighbor( n1 ) );
		
	}

}
