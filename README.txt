****** GIT ******

1) Utworzenie lokalnego brancha na każdy feature ( task )
2) Push, gdy jest gotowy ( do code review, otestowania )
3) Gdy branch zatwierdzony - merge do developa, ew. pozbycie się brancha z repo
4) Po całym sprincie merge developa do mastera 

HINT: 
co zrobić, gdy mamy jakieś zmiany pracując nad danym taskiem, a musimy przejść do innego brancha ?

git add
git stash
* przejscie na innego brancha, zrobienie co trzeba, powrót
git stash pop

*** Komendy: ***

git checkout -b nazwa_brancha_lokalnego origin/nazwa_brancha_zdalnego  // utworzenie i przejscie do nowego brancha wraz z wlaczeniem sledzenia z repozytorium (sciagniecie zmian z danej galezi)
git add nazwa_pliku, git add --all  // przed commitem, dodanie plików do uwzględnienia
git commit -m 'tresc commita' // commit + info co wprowadza
git push -u origin nazwa_brancha // push brancha do repo

** Gdy znajdujemy się na branchu głównym ( np develop) **
git merge nazwa_brancha ( włączenie nazwa_brancha do obecnego brancha )

******
******


Przebieg rozgrywki (skrót)

Akcja gry toczy się przez 2 lata podzielonych na 8 rund. Każdego roku pierwsze 3 rundy to wiosna, lato, jesień, a czwarta zima jest rundą punktacji.

Wiosna, lato i jesień

1. Wykladanie kart akcj
2. Wykladanie kart specjalnych
3. Planowanie akcji i licytacja kolejności graczy
4. Wstalenie wydarzenia
5. Ustalenie kolejności graczy
6. Wykonywanie akcji

Ad.1. Jest 10 kart akcji mówiące o kolejności wykonywania różnych akcji, takich jak budowanie, rekrutowanie itd. Należy je przetasować i następnie wyłożyć. Pierwsze 5 jest odkryte a drugie zakryte. Z każdą kolejną wykonaną akcją odkrywamy jedną zakrytą kartę akcji.

Ad.2. Jest ich 5. Oznaczają różne bonusy, należy je przetasować i  kolejno je odkrywając, umieścić na torze kolejności. W późniejszej części gry gracze będą licytować się o nie oraz o kolejność wykonywania akcji (Pozycja na której została położona dana karta 1-5).

Ad.3. Mamy do wyboru 10a akcji. Na każdą akcję możemy przypisać jedną prowincje. Nie wszystkie akcje muszą zostać przypisane do prowincji. Akcje te to : Budowanie zamku (koszt 3 skrzynie złota), świątyni (2 skrzynie złota), teatru (1 skrzynia zlota) , konfiskata ryżu (zebranie tyle ryzu ile dana prowincja produkuje, położenie na prowincji znacznika buntu), pobór podatków (podobnie jak z ryżem tylko otrzymuje się skrzynie ze złotem) , rekrutacja 5 armii (3 skrzynie złota) , rekrutacja 3 armii (2 skrzynie) , rekrutacja 1 armii + mozliwosc przemieszczenie armii z tej prowincji do swojej sąsiedniej (1 skrzynia złota), Ruch A (przemieszczenie Armii na sasiednią prowincję, może być swoja lub wroga wtedy dochodzi do bitwy) , Ruch B(tak samo jak ruch A). Dodatkowo licytacja kolejności graczy polega na powiedzeniu ile skrzyń chce się wydać na kupienie pierwszeństwa wyboru kolejności i bonusów (kart specjalnych). Można zaplacic od 0-4 skrzyń wojennych albo prowincja. Ta prowincje nie traci się, tylko liczy się za pół i nie można jej użyć do innych akcji. Jak jest remis to decyduje los.

Ad.4. Na poczatku roku losuje się 4 karty wydarzń i je się pokazuje. Podczas każdej pory roku losuje się jedną z nich która będzie obowiązywała podczas pory roku. Posiadają one w sobie informacje o stracie ryżu podczas zimy oraz ograniczeniach lub bonusach jakie będą obowiązywały podczas tej rundy.

Ad.5. Ujawnienie kto za ile licytował i wybór miejsca na torze kolejności. Każde miejsce ma swoją losową kartę specjlaną(bonus).

Ad.6. Wszyscy wykonują akcje w kolejności kart akcji po kolei w zależności od toru kolejności. Tak na prawdę wszyscy mogą robić równolegle wszystko poza ruchem bo tylko wtedy jest interakcja. Runda dobiega końca gdy wszystkie 10 akcji zostaną wykorzystane.

Jezeli znacznik buntu jest na prowincji to podczas obrony nie pomagaja. Jak nie ma to najpierw podczas wojny giną chłopi później wojskowi, pod warunkiem że jacyś losowo pomoga (wypadna z wiezy bitewnej). Bunt wybucha w prowincji gdy na prowincji sa 2 lub wiecej znaczniki buntu. Sila buntu jest rowna ilosci znacznikow. Jezeli bunt zostanie opanowany, czyli jak armie stacjonujace pokonaja bunt, zmniejszamy znacznik buntu do jednego. Jezeli buntownicy wygraja prowincja staje sie neutralna, niszczone sa wsyzstkie budynki a znaczniki buntu usuwane.



Runda Zimowa

Usczuplanie zasobów ryżu o taką ilość jaką wskazuje karta wydarzenia. Następnie gracz musi posiadać po jednym ryżu na każdą prowincję, jeżeli nie ma wtedy wybuchają bunty wg tabeli. Jak wszystkie ewentualne bunty zostana rozstrzygniete nastepuje punktacja. 1pkt za kazdy budynek, 1pkt za kazda prowincje, 3pkt za najwieksza liczbe zamkow w regionie (czyli w danym kolorze prowincji), 2pkt za najwieksza liczbe swiatyn w regionie, 1pkt za najwieksza liczbe teatrow w regionie. W przypadku remisow oboje dostaja punkty pomniejszoną o jeden.



Rozpoczecie:

Napisze w wolnej chwili albo opowiem.